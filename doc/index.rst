Welcome to `scal`'s documentation!
==================================

I use this program about once a year to print a one-page school-year
calendar. But it can be used to represent any calendar.

It is heavily inspired by the simple yet powerful Robert Krause's `calendar
<http://www.texample.net/tikz/examples/a-calender-for-doublesided-din-a4/>`_,
itself using the complex yet powerful Till Tantau's `TikZ
<http://www.ctan.org/pkg/pgf>`_ LaTeX package.

.. contents:: Table of Contents
   :depth: 1
   :local:

Examples
--------

- One-page calendar of a school year

  - English:
    `2022-2023 <https://spalax.frama.io/scal/examples/calendar-en-20222023.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-en-20222023.scl>`__) ;
    `2023-2024 <https://spalax.frama.io/scal/examples/calendar-en-20232024.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-en-20232024.scl>`__).

  - French

    - 2022-2023:
      `Zone A <https://spalax.frama.io/scal/examples/calendar-fr-20222023-A.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20222023-A.scl>`__);
      `Zone B <https://spalax.frama.io/scal/examples/calendar-fr-20222023-B.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20222023-B.scl>`__);
      `Zone C <https://spalax.frama.io/scal/examples/calendar-fr-20222023-C.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20222023-C.scl>`__).

    - 2023-2024:
      `Zone A <https://spalax.frama.io/scal/examples/calendar-fr-20232024-A.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20232024-A.scl>`__);
      `Zone B <https://spalax.frama.io/scal/examples/calendar-fr-20232024-B.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20232024-B.scl>`__);
      `Zone C <https://spalax.frama.io/scal/examples/calendar-fr-20232024-C.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20232024-C.scl>`__).

    - 2024-2025:
      `Zone A <https://spalax.frama.io/scal/examples/calendar-fr-20242025-A.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20242025-A.scl>`__);
      `Zone B <https://spalax.frama.io/scal/examples/calendar-fr-20242025-B.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20242025-B.scl>`__);
      `Zone C <https://spalax.frama.io/scal/examples/calendar-fr-20242025-C.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20242025-C.scl>`__).

    - 2025-2026:
      `Zone A <https://spalax.frama.io/scal/examples/calendar-fr-20252026-A.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20252026-A.scl>`__);
      `Zone B <https://spalax.frama.io/scal/examples/calendar-fr-20252026-B.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20252026-B.scl>`__);
      `Zone C <https://spalax.frama.io/scal/examples/calendar-fr-20252026-C.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/calendar-fr-20252026-C.scl>`__).

- Weekly planners:

  .. admonition:: How to print those planners?
     :class: dropdown

     #. Download the `imposed <https://en.wikipedia.org/wiki/Imposition>`__ version of the files below (or impose them yourself using `pdfimpose <https://framagit.org/spalax/pdfimpose>`__ and ``pdfimpose perfect --group 3 weekly-en-2324.pdf``).
     #. Print them, two-sided, not reversed.
     #. Fold the sheets, that is:

        #. Take the stack of sheets in front of you, the title page being visible, not upside-down, in the bottom right corner of the sheet.
        #. Take the first three sheets of paper, and fold them (together, as if they were one single thick sheet):

           - first vertically: fold the top half of the sheets behind the bottom half;
           - then horizontally: fold the left half of the sheets behin the right half.

           You get a tiny, incomplete book (called a `signature <https://en.wikipedia.org/wiki/Section_(bookbinding)>`__), which cannot be opened yet because of folds. Set it aside.
        #. Repeat the previous step as many times as necessary to fold the whole stack of paper.

     #. Bind the several signatures you got, maybe add a cover.
     #. *Voilà!*

  - English: 
    `2022-2023 <https://spalax.frama.io/scal/examples/weekly-en-20222023.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-en-20222023.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-en-20222023-impose.pdf>`__) ;
    `2022-2023 <https://spalax.frama.io/scal/examples/weekly-en-20232024.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-en-20232024.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-en-20232024-impose.pdf>`__).

  - French

    - 2022-2023:
      `Zone A <https://spalax.frama.io/scal/examples/weekly-fr-20222023-A.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20222023-A.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20222023-A-impose.pdf>`__) ;
      `Zone B <https://spalax.frama.io/scal/examples/weekly-fr-20222023-B.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20222023-B.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20222023-B-impose.pdf>`__) ;
      `Zone C <https://spalax.frama.io/scal/examples/weekly-fr-20222023-C.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20222023-C.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20222023-C-impose.pdf>`__).

    - 2023-2024:
      `Zone A <https://spalax.frama.io/scal/examples/weekly-fr-20232024-A.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20232024-A.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20232024-A-impose.pdf>`__) ;
      `Zone B <https://spalax.frama.io/scal/examples/weekly-fr-20232024-B.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20232024-B.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20232024-B-impose.pdf>`__) ;
      `Zone C <https://spalax.frama.io/scal/examples/weekly-fr-20232024-C.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20232024-C.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20232024-C-impose.pdf>`__).

    - 2024-2025:
      `Zone A <https://spalax.frama.io/scal/examples/weekly-fr-20242025-A.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20242025-A.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20242025-A-impose.pdf>`__) ;
      `Zone B <https://spalax.frama.io/scal/examples/weekly-fr-20242025-B.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20242025-B.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20242025-B-impose.pdf>`__) ;
      `Zone C <https://spalax.frama.io/scal/examples/weekly-fr-20242025-C.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20242025-C.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20242025-C-impose.pdf>`__).

    - 2025-2026:
      `Zone A <https://spalax.frama.io/scal/examples/weekly-fr-20252026-A.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20252026-A.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20252026-A-impose.pdf>`__) ;
      `Zone B <https://spalax.frama.io/scal/examples/weekly-fr-20252026-B.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20252026-B.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20252026-B-impose.pdf>`__) ;
      `Zone C <https://spalax.frama.io/scal/examples/weekly-fr-20252026-C.pdf>`__ (`source <https://framagit.org/spalax/scal/-/raw/main/doc/examples/weekly-fr-20252026-C.scl>`__ ; `imposed <https://spalax.frama.io/scal/examples/weekly-fr-20252026-C-impose.pdf>`__).

Download and install
--------------------

See the `main project page <http://git.framasoft.org/spalax/scal>`_ for
instructions, and `changelog
<https://git.framasoft.org/spalax/scal/blob/main/CHANGELOG.md>`_.

Usage
-----

Note that `scal` only produce the LuaLaTeX code corresponding to the calendar. To get the `pdf` calendar, save the code as a :file:`.tex` file, or pipe the output through ``lualatex``:

.. code-block:: bash

    scal FILE | lualatex

The list of built-in templates is returned by command:

.. code-block:: bash

   scal templates list

Here are the main command line options for `scal`.

.. argparse::
    :module: scal.__main__
    :func: argument_parser
    :prog: scal

Configuration file
------------------

The YAML UTF8-encoded file given in argument contains the information about the calendar.
Here is, for example, the file corresponding to a school year calendar.

.. literalinclude:: examples/calendar-fr-20252026-A.scl

An annotated configuration file (with default values and examples) is available for each template.
For instance, to get this configuration file for template `weekly.tex`, use:

.. code-block:: bash

   scal templates config weekly.tex

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
