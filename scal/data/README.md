Built-in templates are stored here. Each template `foo.tex` must be have two files:
- the LaTeX template `templates/foo.tex` itself, which is a Jinja2 template;
- an example configuration file `config/foo.tex`, written in YAML. It must be a valid configuration file, and should describe the optional and mandatory arguments.
