#!/usr/bin/env python3

# Copyright Louis Paternault 2013-2024
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 1

"""Calendar producer"""

VERSION = "2.4.0"
__AUTHOR__ = "Louis Paternault (spalax@gresille.org)"
__DATE__ = "2011-2024"
__COPYRIGHT__ = f"(C) {__DATE__} Louis Paternault. GNU GPL 3 or later."
